﻿using System.ComponentModel.DataAnnotations;

namespace Mvc.AdminLTE.Models
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}