﻿using System.Collections.Generic;

namespace Mvc.AdminLTE.Models
{
    public class MenuItem
    {
        public MenuItem()
        {
            Text = string.Empty;
            Controller = string.Empty;
            Action = string.Empty;
            Icon = "fa-link";
            Selected = false;
            ChildrenItems = new List<MenuItem>();
            AllowedRoles = string.Empty;
        }

        public string Text { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public bool Selected { get; set; }
        public string AllowedRoles { get; set; }
        public List<MenuItem> ChildrenItems { get; set; }
    }
}