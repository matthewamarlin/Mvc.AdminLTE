﻿using System.ComponentModel.DataAnnotations;

namespace Mvc.AdminLTE.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}