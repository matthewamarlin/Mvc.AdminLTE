﻿using System.ComponentModel.DataAnnotations;

namespace Mvc.AdminLTE.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }
    }
}