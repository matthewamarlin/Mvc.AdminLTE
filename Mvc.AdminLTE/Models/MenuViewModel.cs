﻿using System.Collections.Generic;

namespace Mvc.AdminLTE.Models
{
    public class MenuViewModel
    {
        public string MenuHeader { get; set; }
        public List<MenuItem> Menu { get; set; }
    }
}