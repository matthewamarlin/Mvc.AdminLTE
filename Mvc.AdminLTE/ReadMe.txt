﻿About
	This serves as an example implementation of an ASP.NET MVC website using ASP.NET Identity and the OpenSource AdminLTE UI Framework.

License Type: MIT
	- https://tldrlegal.com/license/mit-license
	- https://opensource.org/licenses/MIT

Requirements:
	- VS2015 Community or higher.
	- A SQL Database (Change Config to match yours)


References:
	- https://almsaeedstudio.com/themes/AdminLTE/index2.html
	- https://www.nuget.org/packages/Microsoft.AspNet.Identity.Samples
	

Helpful Links:
http://stackoverflow.com/questions/35456384/profile-image-in-aspnet-identity
http://www.w3schools.com/howto/howto_js_filter_lists.asp