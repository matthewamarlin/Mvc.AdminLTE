﻿$(function () {
    $(document).ready(function () {
        $("#menu-search").keyup(function () {
            var filter = $(this).val();

            $(".sidebar-menu li:not(.header)").each(function () {

                var li = $(this);

                if (filter === "") {
                    li.css("visibility", "visible");
                    li.fadeIn();
                } else if (li.text().search(new RegExp(filter, "i")) < 0) {
                    li.css("visibility", "hidden");
                    li.fadeOut();
                } else {
                    li.css("visibility", "visible");
                    li.fadeIn();
                }
            });

            $(".sidebar-menu li.treeview").each(function () {

                var tree = $(this);
                var subItems = tree.find("li");
                var ul = tree.find("ul");

                var shouldShow = false;
                subItems.each(function () {
                    if ($(this).css("visibility") !== "hidden") {
                        shouldShow = true;
                    }
                });

                if (filter === "") {
                    tree.css("visibility", "visible");
                    if (ul !== undefined && ul !== null && !tree.hasClass("active")) {
                        ul.css("display", "none");
                    }
                    tree.fadeIn();
                }
                else if (shouldShow) {
                    tree.css("visibility", "visible");
                    if (ul !== undefined && ul !== null) {
                        ul.css("display", "block");
                    }
                    tree.fadeIn();
                }
                else {
                    tree.css("visibility", "hidden");
                    if (ul !== undefined && ul !== null && !tree.hasClass("active")) {
                        ul.css("display", "none");
                    }
                    tree.fadeOut();
                }
            });
        });
    });
});