using System.Collections.Generic;
using System.Web.Mvc;
using Mvc.AdminLTE.Controllers.Filters;
using Mvc.AdminLTE.Models;

namespace Mvc.AdminLTE.Controllers
{
    public class LayoutController : Controller
    {
        private readonly MenuViewModel _menuViewModel = new MenuViewModel()
        {
            MenuHeader = "MENU",
            Menu = new List<MenuItem>
                {
                    new MenuItem{ Text = "Dashboard", Icon = "fa-home", Action = "Index", Controller = "Home",},
                    new MenuItem{ Text = "Admin", Icon = "fa-cogs", Action = "Index", Controller = "Admin", AllowedRoles = "Admin", ChildrenItems = new List<MenuItem>()
                        {
                            new MenuItem { Text = "Roles", Icon = "", Action = "Index", Controller = "RolesAdmin", AllowedRoles = "Admin"},
                            new MenuItem { Text = "Users", Icon = "", Action = "Index", Controller = "UsersAdmin", AllowedRoles = "Admin"},
                            new MenuItem { Text = "Teams", Icon = "", AllowedRoles = "Admin"},
                        }
                    },
                    new MenuItem { Text = "Account", Icon = "fa-user-circle-o", Action = "Index", Controller = "Manage", AllowedRoles = "Public" , ChildrenItems = new List<MenuItem>()
                        {
                            new MenuItem{ Text = "Profile", Action = "Index", Controller = "Manage", AllowedRoles = "Public"},
                            new MenuItem{ Text = "Change Password", Action = "ChangePassword", Controller = "Manage", AllowedRoles = "Public"},
                            new MenuItem{ Text = "Manage External Logins", Action = "ManageLogins", Controller = "Manage", AllowedRoles = "Public"},
                        }
                    },
                }
        };

        [ChildActionOnly]
        [MenuItemAuthorizationActionFilter]
        [MenuItemSelectionActionFilter]
        public ActionResult _Sidebar()
        {
            return PartialView(_menuViewModel);
        }
    }
}