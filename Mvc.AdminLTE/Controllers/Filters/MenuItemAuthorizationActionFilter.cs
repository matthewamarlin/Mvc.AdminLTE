﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc.AdminLTE.Models;

namespace Mvc.AdminLTE.Controllers.Filters
{
    public class MenuItemAuthorizationActionFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Executed After the controller has finished executing but before the ActionResult is returned.
        /// </summary>
        /// <param name="filterContext">The <see cref="ActionExecutedContext"/> of the current Controller's Action</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = (MenuViewModel)filterContext.Controller.ViewData.Model;
            if (viewModel.Menu == null)
                throw new NullReferenceException($"{nameof(MenuViewModel)} object is not initialized");

            viewModel.Menu = FilterByAuthorization(viewModel.Menu);

            base.OnActionExecuted(filterContext);
        }

        private List<MenuItem> FilterByAuthorization(List<MenuItem> menuItems)
        {
            if (menuItems == null) throw new ArgumentNullException(nameof(menuItems));

            return menuItems.Where(menuItem =>
            {
                if (menuItem.ChildrenItems.Any())
                {
                    FilterByAuthorization(menuItem.ChildrenItems);
                }
                
                var isInAppropriateRole = menuItem.AllowedRoles.Split(',').Any(role => HttpContext.Current.User.IsInRole(role));

                return (string.IsNullOrEmpty(menuItem.AllowedRoles) || isInAppropriateRole);

            }).ToList();
        }
    }
}