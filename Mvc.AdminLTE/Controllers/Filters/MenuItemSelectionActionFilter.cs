﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mvc.AdminLTE.Models;

namespace Mvc.AdminLTE.Controllers.Filters
{
    public class MenuItemSelectionActionFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Executed After the controller has finished executing but before the ActionResult is returned.
        /// </summary>
        /// <param name="filterContext">The <see cref="ActionExecutedContext"/></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = (MenuViewModel) filterContext.Controller.ViewData.Model;
            if (viewModel.Menu == null)
                throw new NullReferenceException($"{nameof(MenuViewModel)} object is not initialized");

            var action = filterContext.Controller.ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();
            var controller = filterContext.Controller.ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString();

            viewModel.Menu = SelectActive(viewModel.Menu, action, controller);

            base.OnActionExecuted(filterContext);
        }

        private static List<MenuItem> SelectActive(List<MenuItem> menuItems, string action, string controller)
        {
            foreach (var item in menuItems)
            {
                if (item.ChildrenItems.Any())
                {
                    SelectActive(item.ChildrenItems, action, controller);
                }
                
                if (item.Controller == controller && item.Action == action)
                {
                     item.Selected = true;
                }

                if (item.ChildrenItems.Any(x => x.Selected))
                {
                    item.Selected = true;
                }
            }

            return menuItems;
        }
    }
}