﻿using System.Web.Mvc;

namespace Mvc.AdminLTE.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}